/*
	CA S96
	CA3

	Hadi Safari
	MohammadReza Tayaranian
*/

`timescale 1ns/1ns
`include "microctrl.v"

module tb;
	reg clk, rst;
	microctrl mc(clk, rst);
	integer i, db, rs, tmp, temp;

	initial	begin
		$dumpvars(0);
		clk <= 0;
		rst <= 1;
		#60 rst <= 0;
		// #9000 $finish;
	end

	always #50 clk <= ~clk;

	always @(mc.mp.dp.pc.w)
		if(mc.mp.dp.pc.w >= 16'd100)begin
			$display("reg = %b (%d)\n", mc.mp.dp.acc.w, mc.mp.dp.acc.w);
			$display("\t\tmem\n__________________________________");
			for(i = 0; i < 100; i = i + 1)begin
				tmp = mc.dm.data[i];
				$display("mem[%3d] = %b %b(%3d)", i, tmp[15:13], tmp[13:0], tmp[13:0]);
			end
			for(i = 100; i < 110; i = i + 1)begin 
				$display("mem[%3d] = %16b (%5d)", i, mc.dm.data[i], mc.dm.data[i]);
			end
			$finish;
		end

	initial begin
		db =  $fopen("mem.bin","r");
		for(i = 0; i < 110; i = i + 1)
			rs = $fscanf(db, "%b\n", mc.dm.data[i]);
		$fclose(db);
	end

endmodule
