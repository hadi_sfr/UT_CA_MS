`include "microproc.v"
`include "memory.v"

module  microctrl(input clk, rst);
	wire [15:0] mem_out, mem_in;
	wire [12:0] mem_addr;
	wire mem_ld;
	microproc mp(mem_out, mem_in, mem_addr, mem_ld, rst, clk);
	datamemory dm(mem_out, mem_addr, mem_in, mem_ld, clk);
endmodule
