`include "ALU.v"
`include "myreg.v"

module datapath(output [15:0] mem_in, output [12:0] mem_addr, output [2:0] opcode, input [1:0] alu_sel, alu_b_sel, input alu_a_sel, reg_sel, mem_sel, pc_sel, reg_ld, ir_ld, pc_ld_prim, pc_ld_cond, input [15:0] mem_out, input clk, rst);
	wire zero, pc_ld;
	wire [15:0] alu_out, alu_a, alu_b, reg_out, reg_in, mdr_out, ir_out, pc_out, pc_in;
	ALU tree(zero, alu_out, alu_sel, alu_a, alu_b);
	myreg #(16) pc(pc_out, pc_in, clk, rst, pc_ld);
	myreg #(16) ir(ir_out, mem_out, clk, rst, ir_ld);
	myreg #(16) mdr(mdr_out, mem_out, clk, rst, 1'b1);
	myreg #(16) acc(reg_out, reg_in, clk, rst, reg_ld);
	assign alu_a = alu_a_sel ? reg_out : pc_out;
	assign alu_b = alu_b_sel[1] ? mdr_out : alu_b_sel[0] ? 16'd1 : 16'b0;
	assign reg_in = reg_sel ? mdr_out : alu_out;
	assign pc_in = pc_sel ? ir_out : alu_out;
	assign pc_ld = pc_ld_prim | (pc_ld_cond & zero);
	assign mem_in = reg_out;
	assign mem_addr = (mem_sel ? ir_out[12:0] : pc_out[12:0]);
	assign opcode = ir_out[15:13];
endmodule