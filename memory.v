module datamemory(output [15:0] read_data, input  [12:0] address, input  [15:0] write_data, input write_en, clk);
	reg [15:0] data [0:8192];
	always@(posedge clk)
		if(write_en)
			data[address] = write_data;
	assign read_data = data[address];
endmodule