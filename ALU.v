module ALU(output zero ,output reg signed [15:0] c, input [1:0] operation , input signed [15:0] a, b);
	
	parameter [2:0] ADD = 0, SUB = 1, AND = 2, NOT = 3;
	
	always @( operation ,  b , a) begin
		case (operation)
			AND :  c = a & b;
			NOT :   c = ~a;
			ADD :  c = a + b;
			SUB :  c = a - b;
		endcase
	end
	assign zero = (c) ? 0 : 1;
	
endmodule

