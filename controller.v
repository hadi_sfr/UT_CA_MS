module controller(
	output reg alu_a_sel, reg_sel, mem_sel, pc_sel, reg_ld, ir_ld, pc_ld_prim, pc_ld_cond, mem_ld, output reg [1:0] alu_sel, alu_b_sel,
	input [2:0] opcode, input clk, rst
	);
	parameter [2:0] ADD = 0, SUB = 1, AND = 2, NOT = 3, LDA = 4, STA = 5, JMP = 6, JZ = 7;
	reg [2:0] ps, ns;
	always @(posedge clk)
		ps <= rst ? 0 : ns;
	always @(*) begin
		ns <= ps == 3'd0 ? 3'd1 : ps > 3'd1 ? 3'd0 :
			opcode <= 3'b011 ? 3'd3 : opcode == 3'b100 ? 3'd4 : opcode == 3'b101 ? 3'd5 : 3'd2;
	end
	always@(ps)begin
		case (ps)
			3'd0 : {alu_a_sel, reg_sel, mem_sel, pc_sel, reg_ld, ir_ld, pc_ld_prim, pc_ld_cond, mem_ld, alu_sel, alu_b_sel} <= {1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b1, 1'b1, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b1};
			3'd1 : {alu_a_sel, reg_sel, mem_sel, pc_sel, reg_ld, ir_ld, pc_ld_prim, pc_ld_cond, mem_ld, alu_sel, alu_b_sel} <= {1'b0, 1'b0, 1'b1, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0};
			3'd2 : {alu_a_sel, reg_sel, mem_sel, pc_sel, reg_ld, ir_ld, pc_ld_prim, pc_ld_cond, mem_ld, alu_sel, alu_b_sel} <= {1'b1, 1'b0, 1'b0, 1'b1, 1'b0, 1'b0, opcode == JMP, opcode == JZ, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0};
			3'd3 : {alu_a_sel, reg_sel, mem_sel, pc_sel, reg_ld, ir_ld, pc_ld_prim, pc_ld_cond, mem_ld, alu_sel, alu_b_sel} <= {1'b1, 1'b0, 1'b0, 1'b0, 1'b1, 1'b0, 1'b0, 1'b0, 1'b0, opcode[1:0], 1'b1, 1'b0};
			3'd4 : {alu_a_sel, reg_sel, mem_sel, pc_sel, reg_ld, ir_ld, pc_ld_prim, pc_ld_cond, mem_ld, alu_sel, alu_b_sel} <= {1'b1, 1'b1, 1'b0, 1'b0, 1'b1, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0};
			3'd5 : {alu_a_sel, reg_sel, mem_sel, pc_sel, reg_ld, ir_ld, pc_ld_prim, pc_ld_cond, mem_ld, alu_sel, alu_b_sel} <= {1'b1, 1'b0, 1'b1, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b1, 1'b0, 1'b0, 1'b0, 1'b0};
		endcase
	end
endmodule