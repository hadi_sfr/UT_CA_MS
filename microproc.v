`include "controller.v"
`include "datapath.v"

module microproc(
	input [15:0] mem_out,
	output [15:0] mem_in, output [12:0] mem_addr,
	output mem_ld,
	input rst, clk
	);
	wire [2:0] opcode;
	wire alu_a_sel, reg_sel, mem_sel, pc_sel, reg_ld, ir_ld, pc_ld_prim, pc_ld_cond;
	wire [1:0] alu_sel, alu_b_sel;
	controller ctrl(
		alu_a_sel, reg_sel, mem_sel, pc_sel, reg_ld, ir_ld, pc_ld_prim, pc_ld_cond, mem_ld, alu_sel, alu_b_sel,
		opcode, clk, rst
		);
	datapath dp(mem_in, mem_addr, opcode,
			alu_sel, alu_b_sel, alu_a_sel, reg_sel, mem_sel, pc_sel, reg_ld, ir_ld, pc_ld_prim, pc_ld_cond, mem_out, clk, rst);
endmodule